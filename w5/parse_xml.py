import urllib2
from xml.etree import ElementTree

def parse_xml(url):
    counts = []
    page = urllib2.urlopen(url)
    tree = ElementTree.parse(page) # parse XML tree

    comments = tree.findall('comments/comment') # can be nested

    for comment in comments:
        counts.append(int(comment.find('count').text)) #returns a list

    return sum(counts)

print parse_xml('http://python-data.dr-chuck.net/comments_42.xml')
print parse_xml('http://python-data.dr-chuck.net/comments_302243.xml')