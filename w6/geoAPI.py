import urllib
import json

service_url = "http://maps.googleapis.com/maps/api/geocode/json?"

address = raw_input ("Enter location: ")
url = service_url+urllib.urlencode({'address': address})

print "Retieving",url
url_h = urllib.urlopen(url)
data = url_h.read()
    
try: js = json.loads(str(data))
except: js = None
if 'status' not in js or js['status'] != 'OK':
    print '==== Failure To Retrieve ===='
    print data
else:
    place = js["results"][0]["place_id"]
print 'Place in', place