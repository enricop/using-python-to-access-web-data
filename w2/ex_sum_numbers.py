import urllib
import sys
import re

numbers = []
open("temp.txt", "w").close() # empty the file to clear previous content

url = "http://python-data.dr-chuck.net/regex_sum_302243.txt"
url_h = urllib.urlopen(url).read()
with open("temp.txt","a") as myfile:
    myfile.write(url_h)

fileh = open("temp.txt")
for line in fileh:
    line = line.rstrip() # strip trailing black characters
    numbers.extend(re.findall('[0-9]+', line)) # the method extend() appends the contents of lists.

#to build an iterable us map()
# the map method returns an iterable over the numbers list as integers
print sum(map(int, numbers))