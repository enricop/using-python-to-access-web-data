import urllib
from BeautifulSoup import * #import from local module

url = raw_input('Enter - ')
html = urllib.urlopen(url).read()

soup = BeautifulSoup(html)

tags = soup('span') #span elements has numbers

count = 0
sum = 0
for tag in tags:
    count += 1
    sum += int(tag.contents[0])

print('Count ', count)
print('Sum ', sum)
